#include "SystemOfLinearEquations.hh"
#include "Size.hh"
#include "Matrix.hh"
#include <cmath>
using namespace std;

istream &operator>>(istream &stream, SystemOfLinearEquations &system)
{
    Vector v;
    Matrix m;
    stream >> m >> v;

    system.zapisM(m);
    system.zapisW(v);

    return stream; 
}

ostream &operator<<(ostream &stream,const SystemOfLinearEquations &system)
{
	 
 
  stream << "Macierz Stransponowana"<<endl <<endl<<system.odczytM()<<endl;
  stream <<     "Wyrazy wolne:"     <<endl <<endl<<system.odczytW()<<endl;
  if(system.rodzajRownania)
  {
	  Vector blad_dostep=system.odczytB();
  	stream <<  "Rozwiazanie oznaczone x,y,z =  "             <<system.odczytX()<<endl;
  	stream <<  "Wektor bledu  Ax-b=  " 			 <<system.odczytB()<<endl;
  	stream << "Dlugosc wektora bledu |(Ax-b)| = " <<sqrt(blad_dostep ^ blad_dostep)<<endl <<endl;
  }
  else
  {
	  stream<<"Rownanie sprzeczne wyznacznik=0"<<endl<<endl;
  }

    
  
  return stream;
}

void SystemOfLinearEquations::Obliczenia()
{
	Matrix odpal;
	Vector v;
    double l, Wyznaczniki[SIZE+1];
    Matrix m = this->macierz;
	Wyznaczniki[SIZE] = m.gauss(m);
	if(abs(Wyznaczniki[SIZE])>0.00001)
	{
		for(int i = 0; i < SIZE; i++)
		{
			m = this->macierz;
			m.zapis(i, this->wektor);    //Cramer i kodowanie wektora wyznacznikow
			Wyznaczniki[i] = odpal.gauss(m);
		}

		for(int i = 0; i < SIZE; i++) 
			this->wynik.zapis(i, Wyznaczniki[i]/Wyznaczniki[SIZE]); //Kodowanie wektora wynikowego
		for(int i = 0; i < SIZE; i++)
		{
			l = 0;
			for(int j = 0; j < SIZE; j++)           //kodowanie wektora bledow wynikow
			{
				v = this->macierz.odczyt(j);
				l += v.odczyt(i) * this->wynik.odczyt(j);
			}
			this->blad.zapis(i, (this->wektor.odczyt(i) - l));
		}
		this->rodzajRownania=true;     
	}
	else 
	{	
		this->rodzajRownania=false;
	}
	 
}

