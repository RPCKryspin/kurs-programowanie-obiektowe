#include "Matrix.hh"
#include "Vector.hh"
#include"Size.hh"
#include<cmath>

using namespace std;

ostream &operator<<(ostream &stream, const Matrix &matrix)
{
     int i=0;
    while(i <SIZE)
    {
        stream<<matrix.odczyt(i) <<endl;
		i++;
    }
  return stream;
  
}

istream & operator>>(istream &stream, Matrix &matrix)
{
  Vector vec;
  int i=0;
  while(i < SIZE)
  {
    stream >> vec;
    matrix.zapis(i, vec);
	i++;
  }
  return stream;
}

double Matrix::gauss(Matrix macierz)
{
  	unsigned int wiersz,kolumna,i,j,k;
  	double skala; 
  	int przestawien=0;
  	double wyznacznikG=1;
  	Vector t;
	for(kolumna = 0; kolumna < SIZE; kolumna++)   //operuje na wierszach i elementach dzieki klasie Vector
		for(wiersz = kolumna; wiersz < SIZE-1; wiersz++)
		{
			for(j=kolumna;j<SIZE-1;j++)      //sortuje dopoki element przekontny==0 
				for(i=kolumna;i<SIZE-1;i++)	
				{
					if(abs(macierz(kolumna,kolumna))<0.00001)
					{
						t = macierz[i];
						macierz[i] = macierz[i+1];
						macierz[i+1] = t;
						przestawien++;
					}
					else  
					{
						i=SIZE; 
						j=SIZE;
					}
				}
			if(abs(macierz(kolumna,kolumna))>0.00001)
			{
				skala = macierz(wiersz+1,kolumna) / macierz(kolumna,kolumna) ;
				macierz[wiersz+1] = macierz[wiersz+1] -(macierz[kolumna] * skala);
			}
		}

	for( k=0; k < SIZE; k++) 
		wyznacznikG = wyznacznikG * macierz(k,k);

  	if(przestawien % 2)  // Uwzglednienie liczby przestawien wierszy
		wyznacznikG *= -1; 

	return wyznacznikG;
}
