#include "Vector.hh"
#include<cmath>
#include<iostream>

using namespace std;

ostream &operator<<(ostream &stream, const Vector &vec)
{
    int i=0;
    while(i < SIZE)
    {
        stream << vec.odczyt(i)<<"  " ;
        i++;
    }
      stream << endl;
    return stream;
}

istream &operator>>(istream &stream, Vector &vec)
{
      double wartosci;
      int i=0;                   
  while( i<SIZE)
  {
      stream >> wartosci;
      if(stream.fail())
      {
         return stream;
      }
      else
      {
         vec.zapis(i,wartosci); 
      }
      i++;
  }
  return stream;
}

Vector Vector::operator +(const Vector &vec_2)const
{
     Vector result;                                                   
     int i=0;                                                          
    while(i <SIZE)
    {
        result.zapis(i, this->odczyt(i) + vec_2.odczyt(i));
        i++;
    }
  return result;
}
  
  Vector Vector::operator -(const Vector &vec_2)const
{
     Vector result;
     int i=0;
    while(i <SIZE)
    {
        result.zapis(i, this->odczyt(i) - vec_2.odczyt(i));
        i++;
    }
  return result;
}

Vector Vector::operator *( double skalar)const
{
     Vector result;
     int i=0;
    while(i < SIZE)
    {
        result.zapis(i, this->odczyt(i) * skalar);
        i++;
    }
  return result;
}

double Vector::operator ^(const Vector &vec_2)const
{
     double result_skalarnyIloczyn_wektorow;
     int i=0;
    while(i < SIZE)
    {
        result_skalarnyIloczyn_wektorow += this->odczyt(i) * vec_2.odczyt(i);
        i++;
    }
  return result_skalarnyIloczyn_wektorow;
}

Vector Vector::operator / ( double skalar)const
{
     Vector result;                
      int i = 0; 
        while(i < SIZE)
        {
          if(abs(skalar) < 0.00001)
    			result.zapis(i,0);
		  else
            	result.zapis(i, this->odczyt(i) / skalar);
            	i++; 
        }
    return result;
}

