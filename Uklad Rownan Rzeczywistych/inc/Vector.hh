#pragma once
#include "Size.hh"
#include <iostream>

class Vector
{
  double elementy_wiersza_macierzy[SIZE];
public:

  double odczyt(unsigned int kolumna) const {return this->elementy_wiersza_macierzy[kolumna];}
  void zapis(unsigned int kolumna, const double &skalar) {this->elementy_wiersza_macierzy[kolumna] = skalar;}                                                      
  Vector operator +(const Vector &vec_2)const;
  Vector operator -(const Vector &vec_2)const;
  Vector operator *(double skalar)const;
  double operator ^(const Vector &vec_2)const;
  Vector operator /(double skalar)const;
  double operator[](unsigned int kolumna )const {return elementy_wiersza_macierzy[kolumna];}
  double & operator[](unsigned int kolumna) {return elementy_wiersza_macierzy[kolumna];}

};

std::istream &operator>>(std::istream &stream, Vector &vec);
std::ostream &operator<<(std::ostream &stream, const Vector &vec);


