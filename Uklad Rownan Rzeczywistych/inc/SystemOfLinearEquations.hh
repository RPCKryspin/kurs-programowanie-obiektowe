#pragma once
#include"Size.hh"
#include"Vector.hh"
#include"Matrix.hh"
#include <iostream>


class SystemOfLinearEquations
{
    Matrix macierz;
    Vector wektor;
    Vector wynik;
    Vector blad;
public:
    Matrix odczytM() const {return this->macierz;}
    Vector odczytW()const  {return this->wektor;}
    Vector odczytX() const {return this->wynik;}
    Vector odczytB()const  {return this->blad;}
    void zapisM(const Matrix &mat) {this->macierz = mat;}
    void zapisW(const Vector &vec)  {this->wektor = vec;}
     double rownanie;

    bool rodzajRownania;
    void Obliczenia();
};

/*
  Klasa modeluje Uklad rownan w postaci macierzy współczynników i wektora wynikowego 
  pozwalajac na wyliczenie wektora niewiadomych wartosci. W jej skłąd wchodzą odpowiednie metody 
  pozwalające na odczyty i zapis prywatnych pól jak wykonanie obliczeń wynikowych
*/


std::istream &operator>>(std::istream &stream, SystemOfLinearEquations &system);
std::ostream &operator<<(std::ostream &stream,const SystemOfLinearEquations &system);

/*
 Przeciazenia strumieni pozwalaja na odpowiednie wczytania wartosci 
 do poszczegolnych obiektów tej klasy jak i wypisanie ich
*/
