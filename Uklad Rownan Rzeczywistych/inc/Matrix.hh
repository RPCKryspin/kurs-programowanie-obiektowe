#pragma once
#include "Size.hh"
#include "Vector.hh"
#include <iostream>

class Matrix
{   
  Vector macierz_wektorow[SIZE];
public:
  Vector odczyt(unsigned int kolumna)const {return this->macierz_wektorow[kolumna];}
  void zapis(unsigned int kolumna,const Vector &vec) {this->macierz_wektorow[kolumna]=vec;}
  const Vector& operator[](unsigned int kolumna)const {return macierz_wektorow[kolumna];}
  Vector& operator[](unsigned int kolumna) {return macierz_wektorow[kolumna];}const 
  double operator()(unsigned int wiersz,unsigned int kolumna)const {return macierz_wektorow[wiersz][kolumna];}
  double & operator()(unsigned int wiersz,unsigned int kolumna) {return macierz_wektorow[wiersz][kolumna];}
  double gauss(Matrix macierz);
  
};

std::istream &operator>>(std::istream &stream, Matrix &matrix);
std::ostream &operator<<(std::ostream &stream, const Matrix &matrix);
