#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include"Block.hh"

using namespace std;

Block::Block(string filename)
{
    ifstream inputFile;
    inputFile.open(filename);
    if(!inputFile.is_open())
    {
        cerr << "Unable to load file " << endl;
        return;
    }
    
    Vector3D point;
    while(inputFile >> point)
    {
        points.push_back(point);
    }
    inputFile.close();
}