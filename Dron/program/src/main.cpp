#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cmath>
#include"Vector.hh"
#include "gnuplot_link.hh"
#include "Matrix.hh"
#include "Cuboid.hh"
#include "Block.hh"

#define PI 3.1416

using namespace std;



void menu()
{
    cout << " Menu "                               << endl<<endl;
    cout << " r - ruch na wprost pod danym kontem gora dol" << endl;
    cout << " o - zmiana orientacji w poziomie prawo lewo"    << endl;
    cout << " m - pokaz menu"                      << endl; 
    cout << " k - koniec dzialania programu"       << endl<<endl;; 
}
// Kontrola wprowadzanych skrotow i zadan
void control(char &option)
{
    char noption;
    cin>>noption;
    cout<<endl;
    while(1)
    {
        if(noption!='k' && noption!='m' && noption!='o' && noption!='r')
        {
            cout<<"brak takiej opcji wprowadź raz jeszcze   ";
            cin.ignore(100000, '\n'); 
            cin>>noption;
            cout<<endl;
        }
        else break;
    }
    option=noption;
}

int main()
{
    char option;
    Cuboid cuboid;         
    PzG::GnuplotLink link; 

    link.Init();
    link.AddFilename(kDroneFile.c_str(), PzG::LS_CONTINUOUS, 1);
    link.AddFilename(kSurfaceFile.c_str(), PzG::LS_CONTINUOUS, 1);
    link.AddFilename(kCeilFile.c_str(), PzG::LS_CONTINUOUS, 1);
    link.SetDrawingMode(PzG::DM_3D);
    cuboid.draw(kDroneFile); 
    link.Draw(); // Tutaj gnuplot rysuje, to co zapisaliśmy do pliku

    cout << endl;
    cout << " No to Zaczynamy zabawe ~ Anakin Skywalker "<< endl;
    cout << endl << " Wprowadz opcje menu  "<<endl;
    menu();
    control(option);
    
    while(option!='k')
    {
        switch(option)
        {
            case 'k': option='k';
            break;
            case 'm': menu(); control(option);                  
            break;
            case 'o': 
            {
                Vector3D zerowka;
                double rad;
                cout<<"Wprowadz kat obrotu obrot prawoskretny  ";
                cin>>rad;

                cuboid.javamove(link, zerowka, rad);

                cout<<"Obiektow utworzonych lacznie  ";
                cout<<cuboid.loadNR_ObjectAll()<<endl;
                cout<<"Obiektow obecnie " ;
                cout<< cuboid.loadNR_ObjectNow() <<endl<<endl;

                cout <<endl<< " Wprowadz opcje  "; 
                control(option); 
            } 
            break;
            case 'r': 
            {
                double rad, lenght;
                cout << " Podaj wartosc kata nachylenia (vertical) do powierzchni  ";
                cin >> rad;
                cout << endl << " Podaj odleglosc ruchu wporst  ";
                cin >> lenght; 

                Vector3D translation; //Stara pozycja
                double Vertical_rad = rad * PI / 180;     //nowe kat nachylenia
                double Horizontal_rad = cuboid.loadrad() * PI / 180; // Aktualny kat obrotu 

                translation[0] = lenght * cos(Vertical_rad) * cos(Horizontal_rad);
                translation[1] = lenght * cos(Vertical_rad) * sin(Horizontal_rad);
                translation[2] = lenght * sin(Vertical_rad); 

                cuboid.javamove(link, translation, 0);

                cout<<"Obiektow utworzonych lacznie  ";
                cout<<cuboid.loadNR_ObjectAll()<<endl;
                cout<<"Obiektow obecnie " ;
                cout<< cuboid.loadNR_ObjectNow() << endl<<endl;

                cout << endl<<" Wprowadz opcje ";
                control(option);      
            }
            break;
            
        }
    }
    cout<<"Koniec program"<<endl<<endl;
    return 0;
}


