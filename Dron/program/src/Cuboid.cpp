#include "Cuboid.hh"
#include <fstream>
#include <iostream>
#include <cmath>
#include <chrono>
#include <thread>
#include<string>

#define PI 3.1416
using namespace std;

int Cuboid::ObjCountAll = 0;
chrono::duration<int, milli> timespan(40); 

Cuboid::Cuboid(): angle{0},ObjectNow{0}
{
    ifstream inputFile;
    inputFile.open(kModelCuboid);
    if(!inputFile.is_open())
    {
        cerr << "Unable to load model Cuboid file!" 
             << endl;
        return;
    }
    Vector3D point;
    while(inputFile >> point)
    {
        points.push_back(point);
        ObjCountAll++;
        ObjectNow++;
    }
    inputFile.close();
}

void Cuboid::draw(std::string filename) const
{
    Matrix3D rotation;
    ofstream outputFile;
    outputFile.open(filename);
    if(!outputFile.is_open())
    {
        cerr << "Unable to open drone file!" << endl;
        return;
    }
    makerad_horizontal(rotation);
    for(unsigned i = 0; i < points.size(); ++i)
    {
        outputFile << (rotation * points[i]) + translation << endl;
        if(i % 4 == 3) // triggers after every 4 points
        {
            outputFile << "#\n\n";
        }
        
    }
}

void Cuboid::makerad_horizontal(Matrix3D &rotational_pattern)const
{   
    double rad = this->angle * PI / 180.0;
    rotational_pattern[0][0] = cos(rad);
    rotational_pattern[1][1] = cos(rad);
    rotational_pattern[1][0] = sin(rad);
    rotational_pattern[0][1] =-1*sin(rad);
    rotational_pattern[2][2] = 1;
}

void Cuboid::javamove(PzG::GnuplotLink &link, const Vector3D &new_translation, double new_angle)
{
    Vector3D Actual_localization = loadTrans();
    double Actual_rad = loadrad();
    Vector3D FPS_move = new_translation / 100;
    double FPS_rad = new_angle / 100;
    int i = 0;

    while(i < 100) //FPS-y
    {                                           
        this_thread::sleep_for(timespan);
        saverad(loadrad() + FPS_rad);
        saveTrans(loadTrans() + FPS_move);
        draw(kDroneFile);
        link.Draw();
        i++;
    }
//Zapisany postep na planszy po animacji
}

