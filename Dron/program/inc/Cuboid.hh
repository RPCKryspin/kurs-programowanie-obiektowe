#pragma once
#include <vector>
#include <string>
#include "Vector.hh"
#include "Block.hh"
#include "Matrix.hh"
#include "gnuplot_link.hh"



/**
 * @brief Klasa Prostopadłoscian odwzorowuje zachowanie konkretnej bryly
 * 
 * Ta konkretna bryla to prostopadloscian ktora powstaje z punktow wspolrzednych
 * Dziedziczy z klasy Block, rozszerzając ją o pojęcie kątu obrotu i nachylenia
 */
class Cuboid: public Block
{
    double angle;
    int ObjectNow;
    static int ObjCountAll;

    
public:
    
    /**
     * @brief metoda zwraca liczbe obiektów sumarycznie utowrzonych
     */
     int loadNR_ObjectAll(){return ObjCountAll;};

    /**
     * @brief metoda zwraca liczbe obiektów istniejacych
     */
     int loadNR_ObjectNow(){return ObjectNow;};
    
    /**
     * @brief Konstruktor Prostopadłoscianu
     * 
     * Wizualizuje prostopadłościan wczytując współrzędne punktów
     * modelu z pliku, ustawione są na pozycji początkowej
     * całość punktów traktowana jako jeden obiekt
     */
    Cuboid();
    
    /**
     * @brief Metoda rysująca prostopadłoscian w każdej fazie istnienia
     * 
     * metoda zapisuje współrzędne lokalizacji prostopadłościanu 
     * po obliczeniach do pliku i wykonuje ruch.
     * Łączy również punkty w przestrzeni wizualizując prosotpadłościan
     * Reasumując jest ona odowiedzialna za wizualizacje prostopadłośćianu.
     * Dla odróznienia metoda stowarzyszona link();
     * umożliwia wyświetlenie się w gnuplocie
     * 
     * @param filename plik, do którego mają zostać wpisane punkty.
     */
    void draw(std::string filename) const;
    
    /**
     * @brief Ustawia kąt obrotu prostopadłościanu.
     * 
     * Ustawia kąt obrotu względem osi Z, czyli lewo prawo,
     * czyli horyzontalnie
     * 
     * @param angle Kąt obrotu poziomy.
     */
    void saverad(double rad){angle=rad;}
    
    /**
     * @brief Uzyskuje kąt obrotu prostopadłościanu do wykonania.
     * 
     * @return kąt obrotu poziomy. 
     */
    double loadrad() const {return angle;}
    /**
     * @brief Metoda ustawia kąt obrotu
     * 
     * @param matrix macierz3D zawierająca własności obrotu
     */
    void makerad_horizontal(Matrix3D &rotational_pattern) const;
    
    /**
     * @brief metoda do animacji ruchu
     * 
     * Wiekszosc kodu i sposobu na spowolnienie znaleziona w internecie.
     * Metoda dzieli cały ruch na mniejsze
     * i wyświetla każdy co 60ms.
     * 
     * @param link Link do Gnuplota
     * @param new_translation wektor przesuniecia
     * @param new_angle orientacja katowa
     */
    void javamove(PzG::GnuplotLink &link,const Vector3D &new_translation, double new_angle);

};
