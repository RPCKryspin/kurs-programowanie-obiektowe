#pragma once
#include <iostream>
#include <cmath>

 /**
  * @brief Szablon klasy Wektor
  * 
  * Klasa rekonstruluje zachowanie wektora lub elementu macierzy 
  * lub tez innaczej ujmujac wiersza macierzy
  * 
  * @tparam T wartosc zadana szablonowi np double lub struktura zespolona
  * @tparam Size Rozmaiar Wektora aby powstala dla macierzy kwadratowej
  */
template <typename T, int  Size>
class Vector
{
  T elementy_wiersza_macierzy[Size];
public:
 /**
  * @brief Metoda zczytujaca dany element z wektora
  * 
  * @param kolumna 
  * @return T wartosc zadana szablonowi np double lub struktura zespolona
  */
  T odczyt(unsigned int kolumna) const {return this->elementy_wiersza_macierzy[kolumna];}
  /**
   * @brief 
   * 
   * @param kolumna Metoda zapisujaca dany element do wektora
   * @param skalar wartosc
   */

  void zapis(unsigned int kolumna, const T &skalar) {this->elementy_wiersza_macierzy[kolumna] = skalar;}                                                      
 /**
  * @brief Konstruktor inicjalizujacy zerami 
  * 
  * Potrzebny do zadanie poczatkowego wektora przesuniecia
  */
  Vector() {for(T &konst: elementy_wiersza_macierzy) konst = 0;} 
  /**
   * @brief Przeciazenie dodawania wektorow do siebie
   * 
   * modeluje to sposob dodawania wartosci dwoch roznych obiektow
   * 
   * @param vec_2 wektor z wartosciami
   * @return Vector<T, Size> wektor z nowymi wartosciami
   */
  Vector<T, Size> operator +(const Vector<T, Size> &vec_2)const;
  /**
   * @brief Przeciazenie odejomowania wektorow od siebie
   * 
   * modeluje to sposob dodawania wartosci dwoch roznych obiektow
   * 
   * @param vec_2 wektor z wartosciami
   * @return Vector<T, Size>  wektor z nowymi wartosciami
   */
  Vector<T, Size> operator -(const Vector<T, Size> &vec_2)const;
  /**
   * @brief Przeciazenie mnozenia wektorow przez siebie
   * 
   * modeluje to sposob mnozenia wartosci wektora przez wartosc zwykla
   * inaczej iloczyn wektorowy
   * 
   * @param skalar wartosc
   * @return Vector<T, Size> wektor z nowymi wartosciami
   */
  Vector<T, Size> operator *(T skalar)const;
  /**
   * @brief Przeciazenie dodawania wektoru do siebie
   * 
   * modeluje to sposob mnozenia wartosci wektora przez wartosc zwykla
   * inaczej iloczyn skalarny
   * 
   * @param vec_2 wektor z wartosciami
   * @return T wartosc zadana szablonowi np double lub struktura zespolona
   */
  T operator ^(const Vector<T, Size> &vec_2)const;
  /**
   * @brief Przeciazenie dzielenia wektorow przez siebie 
   * 
   * modeluje to sposob dzielenia wartosci dwoch roznych obiektow
   * 
   * @param skalar wartosc
   * @return Vector<T, Size> wektor z nowymi wartosciami
   */
  Vector<T, Size> operator /(T skalar)const;
  /**
   * @brief przeciazenie operatora przypisania
   * 
   * potrzebne do przypisania wartosci zespolonych do komorki wektora
   * 
   * @param skalar wartosc
   */
  void operator = (T skalar) {for(T &element: elementy_wiersza_macierzy) element = skalar;}
 /**
  * @brief przeciazenie operatora indeksujacego 
  * 
  *  celem jego jest zapisywanie i odczczyt wartosci
  * 
  * @param kolumna 
  * @return T wartosc zadana szablonowi np double lub struktura zespolona
  */
  T operator[](unsigned int kolumna )const {return elementy_wiersza_macierzy[kolumna];}
  /**
   * @brief przeciazenie operatora indeksujacego 
   * 
   * celem jego jest zapisywanie i odczczyt wartosci
   * pozwala na uzyskiwanie wartosci poprzez zmienne
   * 
   * @param kolumna 
   * @return T& zmienna bedaca referencja na wartosc zadana szablonowi np double lub struktura zespolona
   */
  T & operator[](unsigned int kolumna) {return elementy_wiersza_macierzy[kolumna];}

};

 /**
  * @brief Przeciazenie Wyjscia
  * 
  * @tparam T wartosc zadana szablonowi np double lub struktura zespolona (z wektora)
  * @tparam Size Rozmiar Wektora
  * @param stream  Strumien WYJSCIOWY
  * @param vec wektor z ktorego wypiszemy wartosci
  * @return std::ostream& wypisywane wartosci
  */
template <typename T, int  Size>
std::ostream &operator<<(std::ostream &stream, const Vector<T, Size> &vec)
{
    int i=0;
    while(i < Size)
    {
        stream << vec.odczyt(i)<<"  " ;
        i++;
    }
    return stream;
}
 /**
  * @brief Przeciazenie Wejscia
  * 
  * To przeciazenie pozwala wczytywac do wektora liczby
  * 
  * @tparam T wartosc zadana szablonowi np double lub struktura zespolona (z wektora)
  * @tparam Size Rozmiar Wektora
  * @param stream strumien WEJSCIOWY
  * @param vec wektor do ktorego wczytamy liczby
  * @return std::istream&  zakodowany wektor wartosciami zadanymi w szablonie
  */

template <typename T, int  Size>
std::istream &operator>>(std::istream &stream, Vector<T, Size> &vec)
{
      T wartosci;
      int i=0;                   
  while( i<Size)
  {
      stream >> wartosci;
      if(stream.fail())
      {
         return stream;
      }
      else
      {
         vec.zapis(i,wartosci); 
      }
      i++;
  }
  return stream;
}

template <typename T, int  Size> 
Vector<T, Size> Vector<T, Size>::operator +(const Vector<T, Size> &vec_2)const
{
     Vector<T, Size> result;                                                   
     int i=0;                                                          
    while(i <Size)
    {
        result.zapis(i, this->odczyt(i) + vec_2.odczyt(i));
        i++;
    }
  return result;
}

  template <typename T, int  Size>
  Vector<T, Size> Vector<T, Size>::operator -(const Vector<T, Size> &vec_2)const
{
     Vector<T, Size> result;
     int i=0;
    while(i <Size)
    {
        result.zapis(i, this->odczyt(i) - vec_2.odczyt(i));
        i++;
    }
  return result;
}

template <typename T, int  Size>
Vector<T, Size> Vector<T, Size>::operator *( T skalar)const
{
     Vector<T, Size> result;
     int i=0;
    while(i < Size)
    {
        result.zapis(i, this->odczyt(i) * skalar);
        i++;
    }
  return result;
}

template <typename T, int  Size>
T Vector<T, Size>::operator ^(const Vector<T, Size> &vec_2)const
{
     double skalarnyIloczyn=0;
     int i=0;
    while(i < Size)
    {
        skalarnyIloczyn=skalarnyIloczyn+ this->odczyt(i) * vec_2.odczyt(i);
        i++;
    }
  return skalarnyIloczyn;
}

template <typename T, int  Size>
Vector<T, Size>  Vector<T, Size>::operator / ( T skalar)const
{
     Vector<T, Size> result;                
      int i = 0; 
      if(abs(skalar) < 0.00001) return result;
        while(i < Size)
        {
            	result.zapis(i, this->odczyt(i) / skalar);
            	i++; 
        }
    return result;
}
