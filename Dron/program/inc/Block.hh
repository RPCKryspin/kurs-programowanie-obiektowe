#pragma once
#include <vector>
#include <string>
#include "Vector.hh"
#include "Matrix.hh"

using Vector3D=Vector<double,3>;
using Matrix3D=Matrix<double,3>;

const std::string kSurfaceFile("solid/surface.dat");
const std::string kCeilFile("solid/ceil.dat");
const std::string kModelCuboid("solid/model.dat");
const std::string kDroneFile("solid/drone.dat");

/**
 * @brief Klasa Bryła odwzorowuje zachowanie kazdej bryly opisanej w zadaniu programowym
 * 
 * Klasa zawiera metody pozwolajace na przesuwanie wspolrzednych bryły 
 * w zadana lokacje i wczytuje wspolrzedne lokacyjne danej bryły 
 */

class Block
{
protected:
    std::vector<Vector3D> points;
    Vector3D translation;
public:
    /**
     * @brief Metoda zmienia translacje bryły.
     * 
     * @param change Nowy Wektor translacji bryły.
     */
     void saveTrans(const Vector3D &change) {translation = change;}
     /**
     * @brief Metoda pozwoli uzyskac wlasnosci wektora translacji.
     * 
     * @return Vector3D -ukierunkowany ze zwrotem wektor translacji.
     */
     Vector3D loadTrans() const {return translation;}
     /**
     * @brief Konstruktor Bryły.
     * 
     * Wczytuje z pliku współrzędne bryły do dynamicznej tablicy.
     * 
     * @param filename plik ze współrzędnymi punktowymi.
     */
     Block(std::string filename);
     /**
     * @brief Konstruktor bezparametryczny Bryły.
     * 
     * Musiał się pojawić bo prosił o to kompilator.
     */
    Block() {};
};
