#pragma once
#include "Vector.hh"
#include <cmath>
#include <iostream>

/**
 * @brief Szablon klasy Macierz
 * 
 *Szablon Klasy reprezentuje macierz jako wektor ,którego kazdy 
 *element jest innym wektorem na wzor modelu klasy Vector
 *
 * @tparam T - wartosc zadana szablonowi np double lub struktura zespolona
 * @tparam Size -rozmiar wektora
 */

template<typename T, int Size>
class Matrix
{   
  Vector<T, Size> macierz_wektorow[Size];
  double abs(double wartosc) {return sqrt(wartosc*wartosc);}

public:
  /**
   * @brief metoda odczytuje dane z komorki wektora macierzy
   * 
   * @param kolumna 
   * @return wektor liczbowy
   */
  Vector<T, Size> odczyt(unsigned int kolumna)const {return this->macierz_wektorow[kolumna];}
  /**
   * @brief metoda zapisuje dane z komorki wektora macierzy
   * 
   * @param kolumna 
   * @param vec wektor z liczbami jako element macierzy
   */
  void zapis(unsigned int kolumna,const Vector<T, Size> &vec) {this->macierz_wektorow[kolumna]=vec;}
  /**
   * @brief Przeciazenia operatora indeksujacego
   * 
   * dzialaja pokrewnie jak metoda odczyt i zapis
   * nie daje pelnych mozliwosci odczytuje tylko wartosc
   * 
   * @param kolumna 
   * @return wektor z liczbami
   */
  const Vector<T, Size>& operator[](unsigned int kolumna)const {return macierz_wektorow[kolumna];}
  /**
   * @brief Przeciazenia operatora indeksujacego
   * 
   * dzialaja pokrewnie jak metody odczyt i zapis
   * ale daje wieksze mozliwosci poprzez uzyskiwanie zmiennej ktora ma wartosc
   * 
   * @param kolumna - element macierzy czyli Size wektorow z liczbami 
   * @return  wektor z liczbami
   */
  Vector<T, Size>& operator[](unsigned int kolumna) {return macierz_wektorow[kolumna];}const 
  /**
   * @brief Przeciazenia operatora indeksujacego
   * 
   * przeciazenie operatora pozwala uzyskac wartosc z wektora 
   * na poziomie klasy macierzowej
   * 
   * @param wiersz miejsce elementu wektora
   * @param kolumna miejsce elementu macierzy
   * @return T - wartosc zadana szablonowi np double lub struktura zespolona (z wektora)
   */
  T operator()(unsigned int wiersz,unsigned int kolumna)const {return macierz_wektorow[wiersz][kolumna];}
  /**
   * @brief Przeciazenia operatora indeksujacego
   * 
   *  przeciazenie z referencja zwraca zmienna a nie wartosc liczbowa
   *  pozwala to na zapis i odczyt wartosci liczbowych ukrytych pod ta zmienna
  
   * @param wiersz miejsce elementu wektora
   * @param kolumna miejsce elementu macierzy
   * @return T& - zmienna z wartosc zadana szablonowi np double lub struktura zespolona (z wektora)
   */
  T & operator()(unsigned int wiersz,unsigned int kolumna) {return macierz_wektorow[wiersz][kolumna];}
  /**
   * @brief Metoda klasy pozwala liczyc wyznacznik gaussa 
   * 
   * @param macierz czyli 3 wektory z liczbami
   * @return T wartosc zadana szablonowi np double lub struktura zespolona (z wektora)
   */
  T gauss(Matrix<T, Size> macierz);
  /**
   * @brief Kontruktor dla macierzy
   * 
   * Bramka bezpieczenstwa dla zepolenia 3 wektorowdla nowej klasy
   * inicjalizuje macierz zerami
   */
  Matrix(){ for(int i = 0; i < 3; i++) for(int j = 0; j < 3; j++)
            macierz_wektorow[i][j] = 0;}
  /**
   * @brief przeciazenie dla mnozenia macierzy z wektorami
   * 
   * Podporzadkowane pod wzory analityczne oreintacji U(XYZ)
   * 
   * @param vec wektor wspolrzednych dla translacji
   * @return Vector<T, Size>  - czyli wektor ze zmienniona orientacja pozioma
   */
Vector<T, Size> operator*(const Vector<T, Size> &vec) const;
};

/**
 * @brief Metoda i przeciazenie WYJSCIA
 * 
 * Analogiczne maja dzialanie opisane w Vector.hh
 * Tyle ze wczytuje obiekty o cechach wektora do obiektu klasy macierz
 * 
 * @tparam T wartosc zadana szablonowi np double lub struktura zespolona(z wektora)
 * @tparam Size Rozmiar wektora
 * @param stream strumien wyjsciowy
 * @param matrix macierz do wypisania
 * @return std::ostream& wypisuje wektory z wektora macierzowego czyli macierzy
 */

template<typename T, int Size>
std::ostream &operator<<(std::ostream &stream, const Matrix<T, Size> &matrix)
{
     int i=0;
    while(i <Size)
    {
        stream<<matrix.odczyt(i) <<std::endl;
		i++;
    }
  return stream;
}

/**
 * @brief Metoda i przeciazenie WEJSCIA
 * 
 * Analogiczne maja dzialanie opisane w Vector.hh
 * Tyle ze wczytuje obiekty o cechach wektora do obiektu klasy macierz
 * 
 * @tparam T wartosc zadana szablonowi np double lub struktura zespolona (z wektora)
 * @tparam Size Rozmiar wektora
 * @param stream strumien wejsciowy
 * @param matrix macierz do wczytania
 * @return std::istream& wczytane wektory do wektora macierzowego
 */

template<typename T, int Size>
std::istream & operator>>(std::istream &stream, Matrix<T, Size> &matrix)
{
  Vector<T, Size> vec;
  int i=0;
  while(i < Size)
  {
    stream >> vec;
    matrix.zapis(i, vec);
	  i++;
  }
  return stream;
}

template<typename T, int Size>
T Matrix<T, Size>::gauss(Matrix<T, Size> macierz)
{
  unsigned int wiersz,kolumna,i,j,k;
  T skala; 
  int przestawien=0;
  T wyznacznikG;
  wyznacznikG = 1;
  Vector<T, Size> t;
	for(kolumna = 0; kolumna<Size; kolumna++)   
	//operuje na wierszach i elementach dzieki klasie Vector
    for(wiersz = kolumna; wiersz < Size-1; wiersz++)
		{
			//sortuje dopoki element przekontny==0 
			for(j=kolumna; j < Size - 1; j++)      
			for(i=kolumna; i < Size - 1; i++)	
			{
				if(abs(macierz(kolumna,kolumna)) < 0.0000001)
				{
					t = macierz[i];
  				macierz[i] = macierz[i+1];
 					macierz[i+1] = t;
				 	przestawien++;
				}
				else  {i=Size; j=Size;}
			}
			if(abs(macierz(kolumna,kolumna)) > 0.0000001)
			{
        skala = macierz(wiersz+1, kolumna) / macierz(kolumna,kolumna) ;
        macierz[wiersz+1] = macierz[wiersz+1] - (macierz[kolumna] * skala);
			}
    }

	for( k=0; k < Size; k++) wyznacznikG = wyznacznikG*macierz(k,k);
  	if(przestawien % 2) wyznacznikG = wyznacznikG *(-1);  
	  // Uwzglednienie liczby przestawien wierszy
	return wyznacznikG;
}

template<typename T, int Size>
Vector<T, Size> Matrix<T, Size>::operator*(const Vector<T, Size> &vec) const
{
  Vector<T, Size> result;
  for(int i = 0; i < Size; i++)
      for(int j = 0; j < Size; j++)
         result[i] += this->macierz_wektorow[i][j] * vec[j];

    return result;
}



