var searchData=
[
  ['odczyt',['odczyt',['../class_matrix.html#a5d8b00ca37baf08a43f2c797098ae6c1',1,'Matrix::odczyt()'],['../class_vector.html#a2153b98173532bbd5c53a08a49b6d0b1',1,'Vector::odczyt()']]],
  ['operator_28_29',['operator()',['../class_matrix.html#a36dd5ebc2cdba77993066190a118d8a9',1,'Matrix::operator()(unsigned int wiersz, unsigned int kolumna) const'],['../class_matrix.html#a554b8a63f821d76a277cef097a7b157d',1,'Matrix::operator()(unsigned int wiersz, unsigned int kolumna)']]],
  ['operator_2a',['operator*',['../class_matrix.html#a5bd402d9c20033ef1cb8b28016369b8d',1,'Matrix::operator*()'],['../class_vector.html#a13ce1d0236a7f90ef919dd0c4641ee10',1,'Vector::operator*()']]],
  ['operator_2b',['operator+',['../class_vector.html#ac2cbd0891388f4df99b3d46e18ad6f7d',1,'Vector']]],
  ['operator_2d',['operator-',['../class_vector.html#a3757ce78904e909d571a12874383478d',1,'Vector']]],
  ['operator_2f',['operator/',['../class_vector.html#a71ceda7fc8781e1a989a618424ad0dc5',1,'Vector']]],
  ['operator_3d',['operator=',['../class_vector.html#a63d5e639cb11983512cb341615b64b02',1,'Vector']]],
  ['operator_5b_5d',['operator[]',['../class_matrix.html#ab51711a0bd5cdf09446f62a792630953',1,'Matrix::operator[](unsigned int kolumna) const'],['../class_matrix.html#ae4308e07ef24657304aaf4a9cec23c55',1,'Matrix::operator[](unsigned int kolumna)'],['../class_vector.html#a0c9dffc4562ceb5fc97191e4a7a185a8',1,'Vector::operator[](unsigned int kolumna) const'],['../class_vector.html#a8a18806c8f6b59e2e84589212dcf7058',1,'Vector::operator[](unsigned int kolumna)']]],
  ['operator_5e',['operator^',['../class_vector.html#abb6fa7093f8cee48dbda10f6a3f5a8e3',1,'Vector']]]
];
