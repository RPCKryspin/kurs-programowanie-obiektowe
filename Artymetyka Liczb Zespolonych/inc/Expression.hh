#pragma once
#include "Complex.hh"

// Modeluje zbior operatorow arytmetycznych 0,1,2,3
enum Operator
{
    Add,
    Sub,
    Multi,
    Div
};

// Modeluje pojecie dwuargumentowego wyrazenia zespolonego
struct Expression
{
    Complex arg1; // Pierwszy argument wyrazenia arytmetycznego
    Operator op;  // Opertor wyrazenia arytmetycznego
    Complex arg2; // Drugi argument wyrazenia arytmetycznego
};

void Display(Expression dzialanieZesp);
Complex Solve(Expression dzialanieZesp);
