#pragma once
#include<iostream>

// Modeluje pojecie liczby zespolonej
struct Complex
{
    double re; //Pole repezentuje czesc rzeczywista
    double im; //Pole repezentuje czesc urojona
   
};

Complex operator + (Complex arg1, Complex arg2);
Complex operator - (Complex arg1, Complex arg2);
Complex operator * (Complex arg1, Complex arg2);
Complex operator / (Complex arg1, Complex arg2);
Complex operator / (Complex c, double d);
Complex operator += (Complex &arg1, Complex arg2);
Complex operator -= (Complex &arg1, Complex arg2);
bool operator == (Complex a,Complex b);
double Modul_Kw(Complex c);
Complex Im_Sprz(Complex c);
std::istream & operator >> (std::istream &in, Complex &c);
std::ostream & operator << (std::ostream &out, const Complex &c);
int wczytaj(char wyr_alge[], Complex &c);






