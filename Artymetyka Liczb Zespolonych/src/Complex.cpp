#include "Complex.hh"
#include "Expression.hh"
#include<iostream>

Complex operator + (Complex arg1, Complex arg2)
{
    Complex result;

    result.re = arg1.re + arg2.re;
    result.im = arg1.im + arg2.im;
    return result;
}
//Realizuje dodanie dwoch liczb zespolonych

Complex operator - (Complex arg1, Complex arg2)
{
    Complex result;

    result.re = arg1.re - arg2.re;
    result.im = arg1.im - arg2.im;
    return result;
}
//Realizuje odejmowanie dwoch liczb zespolonych

Complex operator * (Complex arg1, Complex arg2)
{
    Complex result;
    result.re = (arg1.re * arg2.re) - (arg1.im * arg2.im);
    result.im = (arg1.re * arg2.im) + (arg1.im * arg2.re); 
    return result;
}
//Realizuje mnozenie dwoch liczb zespolonych

Complex operator / (Complex c, double d)
{
    c.re /= d;
    c.im /= d;                                
    return c;
}
//Realizuje dzielenie Zepolonej przez liczbe 

Complex operator / (Complex arg1, Complex arg2)
{
    Complex result;
    result = arg1 * Im_Sprz(arg2);
    result = result / Modul_Kw(arg2); 

    if(result.im == 0)
        result.im = 0;   
    if(result.re == 0)
        result.re = 0;
// zero ma byc oznaczone jako 0
    return result;
}
//Realizuje dzielenie dwoch liczb zespolonych

Complex Im_Sprz(Complex c)
{
    c.im *= -1;     
    return c;
}
//Funkcja sprzeza Lzesp

double Modul_Kw(Complex c)
{
    c.im *= c.im;    
    c.re *= c.re;
    return (c.re + c.im);   
}
//Funkcja modul do kwadratu

std::istream & operator >> (std::istream &stream, Complex &c)
{
    Complex Lzesp;
    char zn1, zn2, zn3, zn4;
    stream >>zn1 ;
    if(stream.fail()==1 || zn1!='('){stream.setstate(std::ios::failbit); return stream;}
    stream>>Lzesp.re; 
    if(stream.fail()==1){stream.setstate(std::ios::failbit); return stream;}
    stream>>zn2;
    if(stream.fail()==1 || (zn2!='+' && zn2!='-')){stream.setstate(std::ios::failbit); return stream;}
    stream>>Lzesp.im;
    if(stream.fail()==1){stream.setstate(std::ios::failbit); return stream;}
    stream>>zn3;
    if(stream.fail()==1 || zn3!='i'){stream.setstate(std::ios::failbit); return stream;}
    stream>>zn4;
    if(stream.fail()==1 || zn4!=')'){stream.setstate(std::ios::failbit); return stream;}
    
    
    if(zn2 == '-') Lzesp.im =Lzesp.im*-1;                   
    c.re=Lzesp.re;
    c.im=Lzesp.im;       
    return stream;
}
//Przeciazenie strumienia wejscia dla Lzespolnej ( wyniku wprowadzanego przez nas)

std::ostream & operator << (std::ostream &stream, const Complex &c)
{
        if(c.im<0)
            stream << "("<< c.re << c.im << "i" << ")";
        else
            stream << "(" << c.re << "+" << c.im<<"i" << ")";                      
    return stream;
}
//Przeciazenia strumienia wyjscia dla Wypisywania wyniku liczby zespolonej
bool operator == (Complex a,Complex b)       
{
    return (a.re==b.re && a.im==b.im);  
}                  
//przeciazenie dla znaku porownania w celu sprawdzenia odpowiedzi uzytkownika

Complex operator += (Complex &arg1, Complex arg2)
{

   arg1=arg1+arg2;                                         
    return arg1;
}

Complex operator -= (Complex &arg1, Complex arg2)
{

    arg1=arg1-arg2;
    return arg1;
}
//Przeciazenia przypisan

