#include <iostream>
#include <cstring>
#include "Database.hh"
using namespace std;

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        cout << endl;
        cout << " Brak opcji okreslajacej rodzaj testu." << endl;
        cout << " Dopuszczalne nazwy to:  latwy, trudny." << endl;
        cout << endl;
        return 1;
    }


    Database base = {nullptr, 0, 0};
    if (Init(&base, argv[1]) == false)
    {
        cerr << " Inicjalizacja testu nie powiodla sie." << endl;
        return 1;
    }
    
    Expression expression;
    Complex readvalue,solution;
    double score=0;
    cout << endl;
    cout << " Start testu arytmetyki zespolonej: " << argv[1] << endl;
    cout << endl;   
    while (GetNextQuestion(&base, &expression))   
    {
        solution=Solve(expression);
        cout<<"Podaj wynik operacji: "; 
        Display(expression);
        cout<< endl;
        for(int i=1; i<4; i++) // naliczanie prob
        {
            cout << "Twoja odpowiedź: ";
            cin >> readvalue;
            if(cin.fail()==false)
            {          
                if(solution==readvalue)
                {  
                    score++;
                    cout << "Odpowiedz poprawna" << endl;
                }
                else
                {
                    cout << "Blad. Poprawnym wynikiem jest: " << solution << endl;
                }
                break;
            }
            else
            {
                cin.clear();
                cin.ignore(10000,'\n');
                if(i<3 && !strcmp(argv[1], "latwy")) 
                    cout << "Blad zapisu liczby zespolonej. Sprobuj jeszcze raz." << endl;
                else 
                {
                    cout << "Blad zapisu liczby zespolonej. Poprawnym wynikiem jest: "; 
                    cout<< solution << endl; 
                    break;
                }                 
            }
        }
        cout <<endl;
    }
    cout <<"Koniec testu"<<endl;
    cout <<"Ilosc dobrych odpowiedzi:"<<score<<endl;
    cout <<"Ilosc biednych odpowiedzi:"<<4-score<<endl;
    score=(score/4)*100;
    cout<<"Twoj wynik to:"<<score<<"%"<< endl;
    cout << endl;
}


