#include <iostream>
#include <fstream>
#include <cstring>
#include "Vector.hh"
#include "Matrix.hh"
#include "SystemOfLinearEquations.hh"
#include "Complex.hh"

using namespace std;
int main(int argc, char *argv[])
{
	ifstream plik;
  SystemOfLinearEquations<Complex, 5> ZespolonyUklad;
  SystemOfLinearEquations<double, 5> RzeczywistyUklad;

  //Porównanie kwadratu modulu z wyznacznikiem macierzy
  Complex zespolona = {6, -2};
  Matrix<double, 2> macierz;
  macierz[0][0] = zespolona.re;
  macierz[1][1] = zespolona.re;
  macierz[0][1] = zespolona.im;
  macierz[1][0] = zespolona.im * -1;

  cout << "     Kwadrat modulu: " << Modul_Kw(zespolona) << endl;
  cout << "Wyznacznik macierzy: " << macierz.gauss(macierz) << endl << endl; 

	if(argc >= 2)  //Wybieramy rodzaj liczb w macierzy
	{
		if(!strcmp(argv[1], "r"))
		{
			plik.open("r.dat");
			plik >> RzeczywistyUklad;
			RzeczywistyUklad.Obliczenia();
			cout << RzeczywistyUklad;
		}
		else if(!strcmp(argv[1], "z"))
		{
			plik.open("z.dat");
			plik >> ZespolonyUklad;
			ZespolonyUklad.Obliczenia();
			cout << ZespolonyUklad;
		}
	}

	return 0;
}
