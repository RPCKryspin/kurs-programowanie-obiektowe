#include "Complex.hh"
#include<cmath>
#include<iostream>

Complex operator + (Complex arg1, Complex arg2)
{
    Complex result;
    result.re = arg1.re + arg2.re;
    result.im = arg1.im + arg2.im;
    return result;
}
//Realizuje dodanie dwoch liczb zespolonych.

Complex operator - (Complex arg1, Complex arg2)
{
    Complex result;
    result.re = arg1.re - arg2.re;
    result.im = arg1.im - arg2.im;
    return result;
}
//Realizuje odejmowanie dwoch liczb zespolonych.

Complex operator * (Complex arg1, Complex arg2)
{
    Complex result;  
    result.re = (arg1.re * arg2.re) - (arg1.im * arg2.im);
    result.im = (arg1.re * arg2.im) + (arg1.im * arg2.re); 
    return result;
}
//Realizuje mnozenie dwoch liczb zespolonych.

Complex operator / (Complex c, double d)
{
    c.re /= d;
    c.im /= d;                                  
    return c;
}

//realizuje zachowanie dzielnia Lzespolonej przez skalar
Complex operator / (Complex arg1, Complex arg2)
{
    Complex result;
    result = arg1 * Im_Sprz(arg2);
    result = result / Modul_Kw(arg2);  

    if(result.im < 0.00001 && result.im > -0.00001)
        result.im = 0;   
    if(result.re < 0.00001 && result.re >-0.00001)
        result.re = 0;
// zero ma byc oznaczone jako 0
    return result;
}
//Realizuje dzielnie dwoch liczb zespolonych.

Complex Im_Sprz(Complex c)
{
    c.im *= -1;     
    return c;
}
//funkcja sprzezenie liczby zespolonej

double Modul_Kw(Complex c)
{
    c.im = c.im*c.im;    
    c.re = c.re*c.re;
    return (c.re + c.im);
}
//funkcja Modul do kwadratu liczby zespolonej

bool operator == (Complex a,Complex b)       
{
    return (a.re==b.re && a.im==b.im);                    
}
//przeciazenie dla znaku porownania rownoci LZesp

std::istream & operator >> (std::istream &stream, Complex &c)
{
    Complex Lzesp;
    char zn1, zn2, zn3, zn4;
    stream >>zn1 >>Lzesp.re >>zn2>>Lzesp.im>>zn3>>zn4;
        if(zn1 == '(' && (zn2 == '+' || zn2 == '-' ) && zn3 == 'i' && zn4 == ')')
        {
            if(zn2 == '-') c.im =c.im*-1;
            c=Lzesp;
        }                           
    return stream;
}
/*
  Przeciążenie operatora strumienia w celu wczytania liczby zespolonej
  Sprawdza nawet czy wporwadzilismy poprawnie posatc liczby zepolonej oprzez funkcje wczytaj
  wczytywanie z wyrazenia i warunki poprawnego ukladu znakow po zmiennych
*/

std::ostream & operator << (std::ostream &stream, const Complex &c)
{
        if(c.im<0)
        stream << "("<< c.re << c.im << "i" << ")";
        else
        stream << "(" << c.re << "+" << c.im<<"i" << ")";                     
        
    return stream;
}
//przeciazenie wyjscia dla wyswietlenia postaci liczby zespolonej

double abs(Complex c)
{
	return sqrt(Modul_Kw(c));
}
//zalecana przeciazenie dla wartosci bezwzglednej LZesp

void Complex::operator = (const int skalar)
{
    this->re = skalar;
    this->im = 0;
}
//Nadpisanie liczby zespolonej wartoscia rzeczyswista postac zespolona = 0.(0)i

Complex operator * (Complex c, double skalar)
{
    c.re *= skalar;
    c.im *= skalar;
    return c;
}
