#pragma once
#include<iostream>

// Modeluje pojecie liczby zespolonej
struct Complex
{
    double re; //Pole repezentuje czesc rzeczywista. 
    double im; //Pole repezentuje czesc urojona.
    void operator=(const int skalar);
};

Complex operator + (Complex arg1, Complex arg2);
Complex operator - (Complex arg1, Complex arg2);
Complex operator * (Complex arg1, Complex arg2);
Complex operator * (Complex c, double skalar);
Complex operator / (Complex arg1, Complex arg2);
Complex operator / (Complex c, double d);
std::istream & operator >> (std::istream &stream, Complex &c);
std::ostream & operator << (std::ostream &stream, const Complex &c);
bool operator == (Complex a,Complex b);
double Modul_Kw(Complex c);
Complex Im_Sprz(Complex c);
double abs(Complex c);
