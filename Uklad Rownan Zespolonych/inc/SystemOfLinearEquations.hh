#pragma once
#include"Complex.hh"
#include"Vector.hh"
#include"Matrix.hh"
#include <iostream>
#include<cmath>

template <typename T, int  Size>
class SystemOfLinearEquations
{
    Matrix<T, Size> macierz;
    Vector<T, Size> wektor;
    Vector<T, Size> wynik;
    Vector<T, Size> blad;
public:
    Matrix<T, Size> odczytM() const {return this->macierz;}
    Vector<T, Size> odczytW()const  {return this->wektor;}
    Vector<T, Size> odczytX() const {return this->wynik;}
    Vector<T, Size> odczytB()const  {return this->blad;}
    void zapisM(const Matrix<T, Size> &mat) {this->macierz = mat;}
    void zapisW(const Vector<T, Size> &vec)  {this->wektor = vec;}

    bool rodzajRownania;
    void Obliczenia();

};
/*
  Szablon Klasy modeluje Uklad rownan w postaci macierzy współczynników i wektora wynikowego 
  pozwalajac na wyliczenie wektora niewiadomych wartosci. W jej skłąd wchodzą odpowiednie metody 
  pozwalające na odczyty i zapis prywatnych pól jak wykonanie obliczeń wynikowych
*/

template<typename T, int Size>
std::istream &operator>>(std::istream &stream, SystemOfLinearEquations<T, Size> &system)
{
    Vector<T, Size> vec;
    Matrix<T, Size> mat;
    stream >> mat >> vec;

    system.zapisM(mat);
    system.zapisW(vec);

    return stream; 
}

template<typename T, int Size>
std::ostream &operator<<(std::ostream &stream,const SystemOfLinearEquations<T, Size> &system)
{
	 
 
  stream << "Macierz Stransponowana"<<std::endl <<std::endl<<system.odczytM()<<std::endl;
  stream <<     "Wyrazy wolne:"     <<std::endl <<std::endl<<system.odczytW()<<std::endl;
  if(system.rodzajRownania)
  {
  	stream <<  "Rozwiazanie oznaczone x(Size) =  " <<system.odczytX()<<std::endl;
  	stream <<  "Wektor bledu  Ax-b=  " 			 <<system.odczytB()<<std::endl;
  }
  else
  {
	stream<<"Rownanie sprzeczne wyznacznik=0"<<std::endl<<std::endl;
  }

    
  
  return stream;
}
/*
 Przeciazenia strumieni pozwalaja na odpowiednie wczytania wartosci 
 do poszczegolnych obiektów tej klasy jak i wypisanie ich
*/

template<typename T, int Size>
void SystemOfLinearEquations<T, Size>::Obliczenia()
{
	Matrix<T, Size> odpal;
	Vector<T, Size> vec;
    T l, Wyznaczniki[Size+1];
    Matrix<T, Size> mat = this->macierz;
	Wyznaczniki[Size] = odpal.gauss(mat);
	if(abs(Wyznaczniki[Size])>0.00001)
	{
		for(int i = 0; i < Size; i++)
		{
			mat = this->macierz;
			mat.zapis(i, this->wektor); 
            //Cramer i kodowanie wektora wyznacznikow
			Wyznaczniki[i] = odpal.gauss(mat);
		}

		for(int i = 0; i < Size; i++) 
			this->wynik.zapis(i, Wyznaczniki[i]/Wyznaczniki[Size]);   
            //Kodowanie wektora wynikowego
		for(int i = 0; i < Size; i++)
		{
			l = 0;
			for(int j = 0; j < Size; j++) 
            //kodowanie wektora bledow wynikow
			{
				vec = this->macierz.odczyt(j);
				l = l + vec.odczyt(i) * this->wynik.odczyt(j);
			}
			this->blad.zapis(i, (this->wektor.odczyt(i) - l));
		}
		this->rodzajRownania=true;     
	}
	else 
	{	
		this->rodzajRownania=false;
    }
}         
