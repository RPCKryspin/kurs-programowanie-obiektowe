#pragma once
#include "Vector.hh"
#include "Complex.hh"
#include <cmath>
#include <iostream>

template<typename T, int Size>
class Matrix
{   
  Vector<T, Size> macierz_wektorow[Size];
public:
  Vector<T, Size> odczyt(unsigned int kolumna)const {return this->macierz_wektorow[kolumna];}
  void zapis(unsigned int kolumna,const Vector<T, Size> &vec) {this->macierz_wektorow[kolumna]=vec;}

  const Vector<T, Size>& operator[](unsigned int kolumna)const {return macierz_wektorow[kolumna];}
  Vector<T, Size>& operator[](unsigned int kolumna) {return macierz_wektorow[kolumna];}const 
  
  T operator()(unsigned int wiersz,unsigned int kolumna)const {return macierz_wektorow[wiersz][kolumna];}
  T & operator()(unsigned int wiersz,unsigned int kolumna) {return macierz_wektorow[wiersz][kolumna];}
 
  T gauss(Matrix<T, Size> macierz);
};
//Szablon Klasy Macierz

double abs(double wartosc)
{
  return sqrt(wartosc*wartosc);
}

template<typename T, int Size>
std::ostream &operator<<(std::ostream &stream, const Matrix<T, Size> &matrix)
{
     int i=0;
    while(i <Size)
    {
        stream<<matrix.odczyt(i) <<std::endl;
		i++;
    }
  return stream;
}

template<typename T, int Size>
std::istream & operator>>(std::istream &stream, Matrix<T, Size> &matrix)
{
  Vector<T, Size> vec;
  int i=0;
  while(i < Size)
  {
    stream >> vec;
    matrix.zapis(i, vec);
	  i++;
  }
  return stream;
}

template<typename T, int Size>
T Matrix<T, Size>::gauss(Matrix<T, Size> macierz)
{
  unsigned int wiersz,kolumna,i,j,k;
  T skala; 
  int przestawien=0;
  T wyznacznikG;
  wyznacznikG = 1;
  Vector<T, Size> t;
	for(kolumna = 0; kolumna<Size; kolumna++)   
	//operuje na wierszach i elementach dzieki klasie Vector
    for(wiersz = kolumna; wiersz < Size-1; wiersz++)
		{
			//sortuje dopoki element przekontny==0 
			for(j=kolumna; j < Size - 1; j++)      
			for(i=kolumna; i < Size - 1; i++)	
			{
				if(abs(macierz(kolumna,kolumna)) < 0.0000001)
				{
					t = macierz[i];
  				macierz[i] = macierz[i+1];
 					macierz[i+1] = t;
				 	przestawien++;
				}
				else  {i=Size; j=Size;}
			}
			if(abs(macierz(kolumna,kolumna)) > 0.0000001)
			{
        skala = macierz(wiersz+1, kolumna) / macierz(kolumna,kolumna) ;
        macierz[wiersz+1] = macierz[wiersz+1] - (macierz[kolumna] * skala);
			}
    }

	for( k=0; k < Size; k++) wyznacznikG = wyznacznikG*macierz(k,k);
  	if(przestawien % 2) wyznacznikG = wyznacznikG *(-1);  
	  // Uwzglednienie liczby przestawien wierszy
	return wyznacznikG;
}
