#pragma once
#include <iostream>
#include <cmath>
#include"Complex.hh"

template <typename T, int  Size>
class Vector
{
  T elementy_wiersza_macierzy[Size];
public:

  T odczyt(unsigned int kolumna) const {return this->elementy_wiersza_macierzy[kolumna];}
  void zapis(unsigned int kolumna, const T &skalar) {this->elementy_wiersza_macierzy[kolumna] = skalar;}                                                      

  Vector<T, Size> operator +(const Vector<T, Size> &vec_2)const;
  Vector<T, Size> operator -(const Vector<T, Size> &vec_2)const;
  Vector<T, Size> operator *(T skalar)const;
  T operator ^(const Vector<T, Size> &vec_2)const;
  Vector<T, Size> operator /(T skalar)const;
  void operator = (T skalar) {
      for(T &element: elementy_wiersza_macierzy) element = skalar;}

  T operator[](unsigned int kolumna )const {return elementy_wiersza_macierzy[kolumna];}
  T & operator[](unsigned int kolumna) {return elementy_wiersza_macierzy[kolumna];}

};
//Szablon wekotora

template <typename T, int  Size>
std::ostream &operator<<(std::ostream &stream, const Vector<T, Size> &vec)
{
    int i=0;
    while(i < Size)
    {
        stream << vec.odczyt(i)<<"  " ;
        i++;
    }
      stream << std::endl;
    return stream;
}

template <typename T, int  Size>
std::istream &operator>>(std::istream &stream, Vector<T, Size> &vec)
{
      T wartosci;
      int i=0;                   
  while( i<Size)
  {
      stream >> wartosci;
      if(stream.fail())
      {
         return stream;
      }
      else
      {
         vec.zapis(i,wartosci); 
      }
      i++;
  }
  return stream;
}
template <typename T, int  Size> 
Vector<T, Size> Vector<T, Size>::operator +(const Vector<T, Size> &vec_2)const
{
     Vector<T, Size> result;                                                   
     int i=0;                                                          
    while(i <Size)
    {
        result.zapis(i, this->odczyt(i) + vec_2.odczyt(i));
        i++;
    }
  return result;
}

  template <typename T, int  Size>
  Vector<T, Size> Vector<T, Size>::operator -(const Vector<T, Size> &vec_2)const
{
     Vector<T, Size> result;
     int i=0;
    while(i <Size)
    {
        result.zapis(i, this->odczyt(i) - vec_2.odczyt(i));
        i++;
    }
  return result;
}

template <typename T, int  Size>
Vector<T, Size> Vector<T, Size>::operator *( T skalar)const
{
     Vector<T, Size> result;
     int i=0;
    while(i < Size)
    {
        result.zapis(i, this->odczyt(i) * skalar);
        i++;
    }
  return result;
}

template <typename T, int  Size>
T Vector<T, Size>::operator ^(const Vector<T, Size> &vec_2)const
{
     double skalarnyIloczyn=0;
     int i=0;
    while(i < Size)
    {
        skalarnyIloczyn=skalarnyIloczyn+ this->odczyt(i) * vec_2.odczyt(i);
        i++;
    }
  return skalarnyIloczyn;
}

template <typename T, int  Size>
Vector<T, Size>  Vector<T, Size>::operator / ( T skalar)const
{
     Vector<T, Size> result;                
      int i = 0; 
      if(abs(skalar) < 0.00001) return result;
        while(i < Size)
        {
            	result.zapis(i, this->odczyt(i) / skalar);
            	i++; 
        }
    return result;
}

